#include <stdio.h>
#include <stdlib.h>
 
//Algortimo para calcular el mcd
int mcd (int a, int b){
    while(a != b){
        if(a>b)
            a = a-b;
        else
            b = b-a;
    }
    return a;
}
 
//Algoritmo extendido de euclides para sacar el inverso multiplicativo de u mod v
int euclides(int u, int v){
    int q,r,x1,x2,x;
 
    x1=1, x2=0;
    while(u != 0){
        q = v/u;
        x = x2 - q*x1;
 
        r = v-(q*u);
 
        v = u;
        u = r;
        x2 = x1;
        x1 = x;
    }
    x = x2;
    return x;
 
}
 
//Algoritmo para calcular el modulo
int mod (int x, int module){
    int module_number;
    if(x>=0)
        module_number = x - ((x/module) * module);
    else
        module_number = module - ((-1)*x - ((-1 * x)/module)*module);
 
    return module_number;
}
 
//Algoritmo de exponenciacion rapida
int exp_r(int a, int b, int mod){
    int res = 1;
        while(b>0){
            if((b&1)==1)
                res=(a*res)%mod;
        b>>=1;
        a=((a%mod)*(a%mod))%mod;
        }
    return res;
}
 
int cifrar(int mensaje, int e, int n){
    int y;
    //Cipher the text
    y = exp_r(mensaje, e, n);
    return y;
}
 
int decifrar(int p, int q, int d, int e, int y){
    int phi;
    int n;
 
    //Calculate n
    n = p*q;
 
    //Calculate phi of n and calculate mdc with e value
    phi = (p-1) * (q-1);
    if(mcd(e,phi) != 1){
        printf("\nChoose other value for e because the mcd(e,phi) is different than 1\n");
        exit(1);
    }
 
    //To decrypt
    int yp, yq;
    int dp, dq;
    int xp, xq;
    int cp, cq;
 
    //1) Calculate yp and yq
    yp = mod(y, p);
    yq = mod(y, q);
 
    //2) Calculate dp and dq
    dp = mod(d, (p-1));
    dq = mod(d, (q-1));
 
    //3) Calculate
    xp = exp_r(yp, dp, p);
    xq = exp_r(yq, dq, q);
 
    //Chinese residue Theorem
    cp = mod( euclides(q, p), p);
    cq = mod( euclides(p, q), q);
 
    int x;
    x = mod((((q*cp)*xp) + ((p*cq)*xq)), n);
 
    return x;
}
 
int main(int argc, char *argv[]){
//int main(){
    int mensaje =  180;
    int p = 11;
    int q = 23;
    int e = 3;
    int n = p*q;
    int d = 147;
 
    int cifrado;
    cifrado = cifrar(mensaje, e, n);
    printf("El mensaje cifrado %d es: %d\n", mensaje, cifrado);

    int decifrado;
    decifrado = decifrar(p, q, d, e, cifrado);
    printf("El mensaje decifrado de %d es: %d\n", cifrado, decifrado);

    return 0;
}
