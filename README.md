# Service-oriented programning (SOP) #


### OBJECTIVE: ###

To recognize service oriented programming paradigm. 

### LEARNING OUTCOME: ###

The student should be to describe differents between REST and SOAP protocols. 

### Instructions: ###
In this repository there are two kind of dummy services (REST and SOAP)

1. Clone the SOP repository: `$ git clone  https://bitbucket.org/afgiraldofo/sop.git`
2. Install dependencies of python for example, `$ sudo pip install Flask`
3. Execute service.py script in terminal: `$ sop/REST/server.py`. Give permissions if it is necessary by `$ sudo chmod +x sop/REST/server.py`
4. Execute client.py script in other terminal: `$ sop/REST/client.py` in order to consume the service. Give permissions if it is necessary by `$ sudo chmod +x sop/REST/server.py`

Note: Keep terminal of item 3 open all the time.

[REST + Docker](https://www.youtube.com/watch?v=a5td09OWFXA)
