#!/usr/bin/python

import SOAPpy

def hello():
    return "Hello World"

def hello2():
    return "Hello World2"

server = SOAPpy.SOAPServer(("localhost", 8080))
server.registerFunction(hello)
server.registerFunction(hello2)
server.serve_forever()
